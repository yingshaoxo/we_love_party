module github.com/yingshaoxo/we_love_party/management_system/golang_backend_service

go 1.15

require (
	github.com/fmorenovr/gotry v0.0.0-20190819174022-76e4fe5fe9d6 // indirect
	github.com/golang-jwt/jwt/v5 v5.0.0-rc.1 // indirect
	github.com/iancoleman/strcase v0.2.0 // indirect
	github.com/lib/pq v1.10.7
	github.com/redis/go-redis/v9 v9.0.2 // indirect
	github.com/yingshaoxo/gopython v0.0.0-20230306073831-7f1eec85ebd5
	google.golang.org/grpc v1.53.0
	google.golang.org/protobuf v1.28.2-0.20230222093303-bc1253ad3743
)
