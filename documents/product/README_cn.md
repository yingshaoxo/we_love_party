# WeLoveParty

## name
WeLoveParty
我们爱聚会

## needs description
现今人们处于一个尴尬的时代，不能出门、不能实体社交。

只能躲在房间里，偷偷地观察这个世界。

好一点的，参加一个网络社区，获得一点参与感。

同理，我个人也是不喜欢出门的那种人，我最喜欢的还是在线和别人语音聊天，当然，是和聪明人。

我觉得偶尔做一下这个事还是挺好玩的，比如每隔几个月在网上认识一个人。

___

要想做得好，最终我们这个软件还要设置一些屏障，用于过滤一些低端人口。

对于中国人来讲，邮箱本身就是一个门槛。

## about server architecture
我并不想让用户绑定手机号啥的，我只想做一个邮件注册系统。

## about how user can use it
跨平台

## process graph

![graph](processGraph.drawio.svg)