# WeLoveParty privacy

## For now
We will collect your email address, username, gender, 'a picture you choose' as an identity of you.

It will be safely saved on our database.

And we use SSL to encrypt the data we transfer.

## For the future

It may include more data, for example, fingerprint, to make sure you are a human.