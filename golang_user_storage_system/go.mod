module github.com/yingshaoxo/we_love_party/golang_user_storage_system

go 1.15

require (
	github.com/glebarez/sqlite v1.5.0
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/livekit/protocol v1.3.0
	github.com/livekit/server-sdk-go v1.0.6
	github.com/remyoudompheng/bigfft v0.0.0-20220927061507-ef77025ab5aa // indirect
	github.com/rogpeppe/go-internal v1.8.0 // indirect
	github.com/yingshaoxo/gopython v0.0.0-20230117063701-f763b49529e5
	golang.org/x/sys v0.3.0 // indirect
	google.golang.org/grpc v1.51.0
	google.golang.org/protobuf v1.28.1
	gorm.io/driver/postgres v1.4.6 // indirect
	gorm.io/gorm v1.24.2
)
