# This is an internal service that every other internal services can call

## run
```bash
cargo run --bin server --release
```

## test
```bash
cargo test -- --color always --nocapture
```